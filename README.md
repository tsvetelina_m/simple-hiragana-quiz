# Hiragana Quiz App

A very simple hiragana quiz app created using React and Tailwind CSS, following [a tutorial by Tyler Potts](https://www.youtube.com/watch?v=fM3qHaQrRHU). The app shows the user a random kana and prompts them to write the romanji equivalent. In case of an incorrect guess, the app informs the user of the correct answer. The current and maximum streak of the user are kept in local storage and displayed in the app.

Use <em>npm start</em> to run the app. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.